#include <GameSocket.hpp>

void GameSocket::send(std::string data)
{
	auto status = socket.send(data.data(), data.length());
	
	if(status == sf::Socket::Disconnected)
		disconnected = true;
}
///////////////////////////////////////////

std::string GameSocket::read()
{
	std::string data;
	data.resize(256);
	
	std::size_t numReceived;
	
	auto status = socket.receive(const_cast<char *>(data.data()), 256, numReceived);
	
	data.resize(numReceived+1);
	
	if(status == sf::Socket::Disconnected)
	{
		disconnected = true;
		data = "ERROR";
	}	
	
	return data;
}

