#include <GameManager.hpp>

#include <sstream>
#include <iostream>

void GameManager::createGameRoom(User *player1, User *player2)
{
	GameRoom gameRoom;
	gameRoom.players[0] = player1;
	gameRoom.players[1] = player2;
	player1->startGame();
	player2->startGame();
	
	gameRooms.push_back(gameRoom);
	std::cout<<"Start game between: "<<player1->getLogin()<<" and "<<player2->getLogin()<<"\n";
	gameRooms.back().game.init(sf::Vector2i(15, 15), 10, 30, 2, 1.);
}
//////////////////////////////////////////////////////////////

std::string GameManager::parseCommand(std::string &command, User *player)
{
	int playerId = -1;
	int gameRoomId = -1;
	for(size_t i = 0; i < gameRooms.size(); i++)
	{
		if(gameRooms[i].players[0] == player)
		{
			playerId = 0;
			gameRoomId = i;
			break;
		}
		else if(gameRooms[i].players[1] == player)
		{
			playerId = 1;
			gameRoomId = i;
			break;
		}
	}
	
	if(gameRoomId == -1)
		return "NO_GAME_FOUND";
		
	//parse command
	std::stringstream stream;
	stream.str(command);
	
	std::string commandName;
	stream>>commandName;
	
	if(commandName.find("MOVE") != std::string::npos)
	{
		sf::Vector2i pos;
		stream>>pos.x>>pos.y;
		return gameRooms[gameRoomId].game.move(playerId, pos);
	}
	else if(commandName.find("GET_FIELD") != std::string::npos)
		return gameRooms[gameRoomId].game.getField(playerId);
	else if(commandName.find("SHOOT") != std::string::npos)
		return gameRooms[gameRoomId].game.shoot(playerId);
	else if(commandName.find("GET_ROUND") != std::string::npos)
		return gameRooms[gameRoomId].game.getRound();
	else if(commandName.find("GET_AMMO") != std::string::npos)
		return gameRooms[gameRoomId].game.getAmmo(playerId);
	else if(commandName.find("GET_POINTS")  != std::string::npos)
		return gameRooms[gameRoomId].game.getPoints(playerId);
	else if(commandName.find("GAME_INFO")  != std::string::npos)
		return gameRooms[gameRoomId].game.getGameInfo();
	else
		return "INVALID_COMMAND";
}
////////////////////////////////////////////////////////////////

void GameManager::update()
{
	for(auto it = gameRooms.begin(); it != gameRooms.end(); )
	{
		int status = it->game.update();
		if(status == 0)
		{
			it->players[0]->endTurn("OK");
			it->players[1]->endTurn("OK");
		}
		if(status == 1)
		{
			std::cout<<"End game between: "<<it->players[0]->getLogin()<<" and "<<
				it->players[1]->getLogin()<<"\n";
			std::cout<<"Final score: "<<std::to_string(it->game.players[0].points)<<" "<<
				std::to_string(it->game.players[1].points)<<"\n";
			
			it->players[0]->endTurn("GAME_OVER\n" + std::to_string(it->game.players[0].points) + "\n" + std::to_string(it->game.players[1].points));
			it->players[1]->endTurn("GAME_OVER\n" + std::to_string(it->game.players[1].points) + "\n" + std::to_string(it->game.players[0].points));
			it = gameRooms.erase(it);
		}
		else
			it++;
	}
}
