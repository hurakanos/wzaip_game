#include <UserManager.hpp>

#include <sstream>

#include <User.hpp>

UserManager::UserManager(unsigned short port, GameManager *gameManager)
{
	this->port = port;
	listener.listen(port);
	selector.add(listener);
	this->gameManager = gameManager;
}
/////////////////////////////////////////////

void UserManager::update()
{

//remove dissconnected sockets
	for(auto it = sockets.begin(); it != sockets.end(); )
	{
		if((*it)->isDisconnected())
		{
			selector.remove((*it)->socket);
			socketUserMap.erase((*it).get());
			it = sockets.erase(it);
		}
		else
			it++;
	}
	
//check for ready sockets
	if(selector.wait(sf::seconds(0.5)))
	{
		if(selector.isReady(listener))
		{
			sockets.push_back(std::make_unique<GameSocket>());
			if(listener.accept( sockets.back()->socket ) == sf::Socket::Done)
			{
				selector.add(sockets.back()->socket);
				sockets.back()->send("LOGIN");
			}
			else
				users.pop_back();
		}
		for(auto &socket: sockets)
		{
			if(selector.isReady(socket->socket))
			{
				if(socketUserMap.find(socket.get()) != socketUserMap.end())//socket after login
				{
					socketUserMap[socket.get()]->receive(socket.get());
				}
				else//try to login socket
				{
					handleLogin(socket.get());
				}
			}
		}
	}
}
/////////////////////////////////////////////////////////////

void UserManager::handleLogin(GameSocket *socket)
{
	std::string credentials = socket->read();
	
	if(credentials.find("ERROR") != std::string::npos)
		return;
		
	std::stringstream stream;
	stream.str(credentials);
	
	std::string login, password;
	stream>>login>>password;
	if(password.back() == 0)//there is null character if string for whatever reason
		password.pop_back();
	
	for(auto &user: users)
	{
		if(user->login == login && user->password == password)
		{
			socketUserMap[socket] = user.get();
			if(user->inGame)
				socket->send("OK");//player already in game, ready for commands
			
			//if not in game we wait for first login to start next game
			if(!user->waitForStart)
				user->waitForStart = true;
			
			return;
		}
	}
	
	socket->send("BAD_LOGIN_OR_PASSWORD");
}
////////////////////////////////////////////////////////////

std::vector<GameSocket*> UserManager::getUserSockets(User *user)
{
	std::vector<GameSocket*> sockets;
	
	for(auto value: socketUserMap)
	{
		if(value.second == user)
			sockets.push_back(value.first);
	}
	
	return sockets;
}
//////////////////////////////////////////////////////////

void UserManager::addUser(std::string login, std::string password)
{
	users.push_back(std::make_unique<User>());
	users.back()->login = login;
	users.back()->password = password;
	users.back()->userManager = this;
	users.back()->gameManager = gameManager;
}
