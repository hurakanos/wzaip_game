#include <User.hpp>

#include <exception>

#include <GameManager.hpp>
#include <UserManager.hpp>

void User::receive(GameSocket *socket)
{
	if(socket->isDisconnected())
		return;

	std::string command = socket->read();
	if(command.find("ERROR") != std::string::npos)
		return;

	
	if(inGame == false)
	{		
		socket->send("NOT_IN_GAME");
		return;
	}
	
	if(command.find("WAIT") != std::string::npos)
	{
		socket->waiting() = true;
		socket->send("OK");
		return;
	}
		
	
	std::string answer = gameManager->parseCommand(command, this);
	
	socket->send(answer);
	
	if(answer.find("NO_GAME_FOUND") != std::string::npos)
	{
		inGame = false;
		socket->disconnect();//if no game then it's some server errors
		waitForStart = false;
	}
}
/////////////////////////////////////////////////////////////////

void User::endTurn(std::string status)
{
	if(status.find("GAME_OVER") != std::string::npos)
		inGame = false;
	
	for(auto socket: userManager->getUserSockets(this))
	{
		if(socket->waiting())
		{
			socket->waiting() = false;
			socket->send(status);
		}
		if(!inGame)
			socket->disconnect();
	}
}
/////////////////////////////////////////////////////////////////

void User::startGame()
{
	inGame = true;
	waitForStart = false;

	for(auto socket: userManager->getUserSockets(this))
		socket->send("OK");
}
