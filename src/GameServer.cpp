#include <GameServer.hpp>
#include <string>
#include <sstream>
#include <cstring>
#include <cmath>
#include <cstdlib>
#include <ctime>
#define RANDOM_COEF 20
#include <iostream>

GameServer::GameServer(sf::Vector2i fieldSize, unsigned short port):
SocketServer(port), fieldSize(fieldSize)
{
	//empty
}
////////////////////////////

GameServer::~GameServer()
{
	if(field != nullptr)
		delete[] field;
}
/////////////////////////////////////////////////

void GameServer::executeCommand(unsigned player)
{
	std::string command = read(player);
	
	std::stringstream str;
	str.str(command);
	
	str>>command;
	
	if(command == "MOVE")
	{
		sf::Vector2i mov, shoot;
		str>>mov.x>>mov.y>>shoot.x>>shoot.y;
		if( players[player].move(mov, shoot) == 0 )
		{
			players[player].moveThisTurn = true;
			send("OK", player);
		}
		else
			send("INVALID_PARAMETERS", player);
	}
	//player should be waiting at the end of turn becaouse it's easier that way
	else if(command == "WAIT")
	{
		players[player].waiting = true;
		send("OK", player);
	}		
	else if(command == "GET_FIELD")
	{
		send(getField(player), player);
	}		
	else
		send("UNRECOGNIZED_COMMAND", player);
}
//////////////////////////////////////////////////////

int GameServer::Player::move(sf::Vector2i dir, sf::Vector2i shoot)
{
	if( (dir.x < -1 || dir.x > 1) && (dir.y < -1 || dir.y > 1) )
		return 1;
	if( (shoot.x < -1 || shoot.x > 1) && (shoot.y < -1 || shoot.y > 1) )
		return 1;
	
	sf::Vector2i nextPos = currentPosition + dir;
	if( (nextPos.x < 0 || nextPos.x > game->fieldSize.x) &&
		(nextPos.y < 0 || nextPos.y > game->fieldSize.y) )
			return 1;
	
	if(game->field[nextPos.y * game->fieldSize.x + nextPos.x] != 0)
		return 1;
	
	moveDirection = dir;
	shootDirection = shoot;
	
	return 0;
}
////////////////////////////////////////////////////

std::string GameServer::getField(unsigned firstPlayer)
{
	unsigned secondPlayer = firstPlayer == 0? 1 : 0;
	std::stringstream str;
	
	str<<"OK\n";
	str<<players[firstPlayer].currentPosition.x<<" "<<players[firstPlayer].currentPosition.y<<"\n";
	str<<players[secondPlayer].currentPosition.x<<" "<<players[secondPlayer].currentPosition.y<<"\n";
	
	str<<obsticleCount<<"\n";
	for(int i = 0; i < fieldSize.y; i++)
		for(int j = 0; j < fieldSize.x; j++)
			if(field[i * fieldSize.x + j] == 1)
				str<<j<<" "<<i<<"\n";
				
	return str.str();
}
//////////////////////////////////////////////////

void GameServer::checkCollision(unsigned player, float &distance, int &collisionType, sf::Vector2i &pos)
{
	distance = 0;
	std::cout<<players[player].currentPosition.x<<" "<<players[player].currentPosition.y<<
	" "<<players[player].shootDirection.x<<" "<<players[player].shootDirection.y<<std::endl;
	if(players[player].shootDirection != sf::Vector2i(0, 0))
	{
		for(auto curPos = players[player].currentPosition + players[player].shootDirection;
		curPos.x >= 0 && curPos.y >= 0 && curPos.x < fieldSize.x && curPos.y < fieldSize.y;
		curPos += players[player].shootDirection)
		{
			std::cout<<curPos.x<<" "<<curPos.y<<std::endl;
			if(field[curPos.y * fieldSize.x + curPos.x] != 0)
			{
				distance = (curPos.x - players[player].currentPosition.x) * 
				(curPos.x - players[player].currentPosition.x) +
				(curPos.y - players[player].currentPosition.y) *
				(curPos.y - players[player].currentPosition.y);
				
				distance = sqrt(distance);
				
				pos = curPos;
				collisionType = field[curPos.y * fieldSize.x + curPos.x];
				break;
			}
		}
	}
}

///////////////////////////////////////////////////

void GameServer::endTurn()
{
	for(int i = 0; i < 2; i++)
	{
		players[i].currentPosition += players[i].moveDirection;
		players[i].moveThisTurn = false;
		players[i].waiting = false;
	}
	
	float distance1, distance2;
	int collisionType1, collisionType2;
	sf::Vector2i pos1, pos2;
	
	checkCollision(0, distance1, collisionType1, pos1);
	checkCollision(1, distance2, collisionType2, pos2);
	std::cout<<distance1<<" "<<collisionType1<<" "<<pos1.x<<" "<<pos1.y<<"\n";
	std::cout<<distance2<<" "<<collisionType2<<" "<<pos2.x<<" "<<pos2.y<<"\n";
	if(distance1 != 0 && (distance1 < distance2 || distance2 == 0))
	{
		if(collisionType1 == 1)
		{
			field[pos1.y * fieldSize.x + pos1.x] = 0;
			obsticleCount--;
			checkCollision(1, distance2, collisionType2, pos2);
		}
		else if(collisionType1 == 2)
			winner += 1;
			
		//check second collsion	
		if(collisionType2 == 1)
		{
			field[pos2.y * fieldSize.x + pos2.x] = 0;
			obsticleCount--;
		}
		else if(collisionType2 == 2)
			winner += 2;
	}
	else if(distance2 != 0)
	{
		if(collisionType2 == 1)
		{
			field[pos2.y * fieldSize.x + pos2.x] = 0;
			obsticleCount--;
			checkCollision(0, distance1, collisionType1, pos1);
		}
		else if(collisionType2 == 2)
			winner += 2;
			
		//check second collsion	
		if(collisionType1 == 1)
		{
			field[pos1.y * fieldSize.x + pos1.x] = 0;
			obsticleCount--;
		}
		else if(collisionType1 == 2)
			winner += 1;
	}
}

//////////////////////////////////////////////////////////

void GameServer::initialize()
{
	//initialize game field
	if(field != nullptr)
		delete[] field;
	field = new char[fieldSize.x * fieldSize.y];
	obsticleCount = 0;
	
	srand(time(0));
	
	for(int i = 0; i < fieldSize.x * fieldSize.y; i++)
	{
		if((rand() % RANDOM_COEF) == 1)
		{
			field[i] = 1;
			obsticleCount++;
		}
		else
			field[i] = 0;
	}
	
	//initialize player structures
	for(int i = 0; i < 2; i++)
	{
		players[i].moveDirection.x = 0;
		players[i].moveDirection.y = 0;
		players[i].shootDirection.x = 0;
		players[i].shootDirection.y = 0;
		players[i].moveThisTurn = false;
		players[i].waiting = false;
		players[i].game = this;
	}
	players[0].currentPosition.x = 0;
	players[0].currentPosition.y = 0;
	players[1].currentPosition.x = fieldSize.x-1;
	players[1].currentPosition.y = fieldSize.y-1;
	field[0] = 2;
	field[players[1].currentPosition.y * fieldSize.x + players[1].currentPosition.x] = 2;

	//listen for connection and wait for players	
	sf::SocketSelector selector;
	
	listen();
	selector.add(listener);
	
	int connected{0};
	
	while(true)
	{
		if(selector.wait())
		{
			if(selector.isReady(listener))
			{
				if(connected < 2 && connect(connected))
					selector.add(socket[connected++]);
			}
			else
			{
				if(connected > 0 && selector.isReady(socket[0]))
				{
					if(read(0) == "START")
					{
						players[0].waiting = true;
						send("OK", 0);
					}
					else
					{
						send("GAME_NOT_STARTED", 0);
					}
				}
				if(connected > 1 && selector.isReady(socket[1]))
				{
					if(read(1) == "START")
					{
						players[1].waiting = true;
						send("OK", 1);
					}
					else
					{
						send("GAME_NOT_STARTED", 1);
					}
				}
				
				if(players[0].waiting == true && players[1].waiting == true)
					break;
			}
		}
	}
	
	//prepare for game and inform players about start
	players[0].waiting = false;
	players[1].waiting = false;
	selector.remove(listener);
	stopListening();
	send("OK", 0);
	send("OK", 1);
	
}
/////////////////////////////////////////////////////////////

void GameServer::run()
{
	sf::SocketSelector selector;
	
	selector.add(socket[0]);
	selector.add(socket[1]);
	
	//main loop of the game
	while(true)
	{
		if(selector.wait())
		{
			if(selector.isReady(socket[0]))
				executeCommand(0);
			if(selector.isReady(socket[1]))
				executeCommand(1);
		}
		if(players[0].waiting == true && players[1].waiting == true)
		{
			endTurn();
			if(winner == 0)//goto next turn
			{
				send("OK", 0);
				send("OK", 1);
				continue;
			}
			if(winner == 1)//player 1 won
			{
				send("WINNER", 0);
				send("LOSER", 1);
				break;
			}
			if(winner == 2)//player 2 won
			{
				send("WINNER", 1);
				send("LOSER", 0);
				break;
			}
			if(winner == 3)
			{
				send("TIE", 0);
				send("TIE", 1);
				break;
			}
		}
	}
}
//////////////////////////////////////////////////////////

std::string GameServer::read(size_t socketId)
{
	std::string data = SocketServer::read(socketId);
	
	auto pos = data.begin();
	
	for( ; pos != data.end(); pos++)
		if( *pos < 32 || *pos > 126)
			break;
			
	data.erase(pos, data.end());
	
	return data;
}
