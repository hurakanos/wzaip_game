#include <SocketServer.hpp>
#include <iostream>

SocketServer::SocketServer(unsigned short port):
	port(port)
{
	//empty body
}
////////////////////////////////////////////

bool SocketServer::listen()
{
	socket[0].disconnect();
	socket[1].disconnect();
	
	auto status = listener.listen(port);
	
	if(status != sf::Socket::Done)
		return false;
	else
		return true;
}
///////////////////////////////////////////

void SocketServer::stopListening()
{
	listener.close();
}
////////////////////////////////////////

bool SocketServer::connect(size_t socketId)
{
	auto status = listener.accept(socket[socketId]);
	
	if(status != sf::Socket::Done)
		return false;
	else
		return true;
}
///////////////////////////////////////////////

bool SocketServer::send(std::string data, size_t socketId)
{
	auto status = socket[socketId].send(data.data(), data.length());
	std::cout<<"Sent: "<<data<<"\nto id: "<<socketId<<std::endl;
	
	if(status != sf::Socket::Done)
		return false;
	else
		return true;
}
////////////////////////////////////////////

std::string SocketServer::read(size_t socketId)
{
	std::string data;
	data.resize(256);
	
	std::size_t numReceived;
	
	auto status = socket[socketId].receive(const_cast<char *>(data.data()), 256, numReceived);
	
	data.resize(numReceived+1);
	
	if(status != sf::Socket::Done)
		data = "Error!!!";
		
	std::cout<<"Read: "<<data<<"\nfrom id: "<<socketId<<std::endl;
	
	return data;
}
/////////////////////////////////////////////////////////////////

const sf::TcpSocket &SocketServer::operator[](size_t socketId)const
{
	return socket[socketId];
}
