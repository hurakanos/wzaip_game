#include <SocketClient.hpp>

#include <iostream>

SocketClient::SocketClient(std::string ipAddress, unsigned short port):
	ipAddress(ipAddress), port(port)
{
	//empty body
}
///////////////////////////////

bool SocketClient::connect()
{
	auto status = socket.connect(ipAddress, port);
	
	if(status != sf::Socket::Done)
		return false;
	else
		return true;
}
//////////////////////////////////////////

bool SocketClient::send(std::string data)
{
	auto status = socket.send(data.data(), data.length());
	
	std::cout<<"\nSent:\n"<<data<<"\n";
	
	if(status != sf::Socket::Done)
		return false;
	else
		return true;
}
///////////////////////////////////////////

std::string SocketClient::read()
{
	std::string data;
	data.resize(256);
	
	std::size_t numReceived;
	
	auto status = socket.receive(const_cast<char *>(data.data()), 256, numReceived);
	
	data.resize(numReceived+1);
	
	if(status != sf::Socket::Done)
		data = "Error!!!";
	
	std::cout<<"\nReceived:\n"<<data<<"\n";
		
	return data;
}
