#include <SingleGame.hpp>

#include <cmath>
#include <cstdlib>
#include <ctime>
#define RANDOM_COEF 20


void SingleGame::init(sf::Vector2i size, int initAmmo, 
int roundCount, int moveDistance, float roundDuration)
{
	this->fieldSize = size;
	this->initialAmmo = initAmmo;
	this->roundCount = roundCount;
	this->moveDistance = moveDistance;
	this->roundDuration = roundDuration;
	
	players[0].position = sf::Vector2i(0, 0);
	players[0].ammo = initialAmmo;
	players[0].nextMove = sf::Vector2i(-1, -1);
	players[0].ifShoot = false;
	players[0].points = 0;
	
	players[1].position = sf::Vector2i(fieldSize.x-1, fieldSize.y-1);
	players[1].ammo = initialAmmo;
	players[1].nextMove = sf::Vector2i(-1, -1);
	players[1].ifShoot = false;
	players[1].points = 0;
	
	
	//set field
	obsticleCount = 0;
	srand(time(0));
	
	field.resize(fieldSize.x * fieldSize.y);
	for(size_t i = 1; i < fieldSize.x * fieldSize.y - 2; i++)
	{
		if(rand() % RANDOM_COEF == 1)
		{
			field[i] = Field::Obsticle;
			obsticleCount++;
		}
		else
			field[i] = Field::Empty;
	}
		
	field[0] = Field::Player1;
	field[fieldSize.x * fieldSize.y - 1] = Field::Player2;
	
	currentRound = 0;
	roundTimer.restart();
}
/////////////////////////////////////////////////////////////////

int SingleGame::update()
{
	if(roundTimer.getElapsedTime().asSeconds() < roundDuration)
		return -1;

	if(players[0].nextMove != sf::Vector2i(-1, -1))
	{
		field[players[0].position.x + players[0].position.y * fieldSize.x] = Field::Empty;
		players[0].position = players[0].nextMove;
		field[players[0].position.x + players[0].position.y * fieldSize.x] = Field::Player1;
		players[0].nextMove = sf::Vector2i(-1, -1);
	}
	
	if(players[1].nextMove != sf::Vector2i(-1, -1))
	{
		field[players[1].position.x + players[1].position.y * fieldSize.x] = Field::Empty;
		players[1].position = players[1].nextMove;
		field[players[1].position.x + players[1].position.y * fieldSize.x] = Field::Player2;
		players[1].nextMove = sf::Vector2i(-1, -1);
	}
	
	sf::Vector2i hit1, hit2;
	
	hit1 = getVisible(players[0].position, players[1].position);
	hit2 = getVisible(players[1].position, players[0].position);
	
	if(players[0].ifShoot == true)
	{
		players[0].ammo--;
		players[0].ifShoot = false;
		if(hit1 == players[1].position)
			players[0].points++;
		else if(hit1.x != -1)
		{
			field[hit1.x + hit1.y * fieldSize.x] = Field::Empty;
			obsticleCount--;
		}
	}
	if(players[1].ifShoot == true)
	{
		players[1].ammo--;
		players[1].ifShoot = false;
		if(hit2 == players[0].position)
			players[1].points++;
		else if(hit2.x != -1 && hit1 != hit2)
		{
			field[hit2.x + hit2.y * fieldSize.x] = Field::Empty;
			obsticleCount--;
		}
	}
	
	currentRound++;
	
	if(currentRound == roundCount || players[0].ammo == 0 && players[1].ammo == 0)
		return 1;
	
	roundTimer.restart();
	
	return 0;
}
////////////////////////////////////////////////////////////////

std::string SingleGame::getField(int player)
{
	std::string result = "OK\n";
	int player2 = player == 0? 1: 0;
	
	result += std::to_string(fieldSize.x) + " " + std::to_string(fieldSize.y) + "\n";
	
	result += std::to_string(players[player].position.x) + " " + std::to_string(players[player].position.y) + "\n";
	result += std::to_string(players[player2].position.x) + " " + std::to_string(players[player2].position.y) + "\n";
	
	result += std::to_string(obsticleCount) + "\n";
	for(int y = 0; y < fieldSize.y; y++)
		for(int x = 0; x < fieldSize.x; x++)
			if(field[x + y * fieldSize.x] == Field::Obsticle)
				result += std::to_string(x) + " " + std::to_string(y) + "\n";
	
	return result;
}
///////////////////////////////////////////////////////////

std::string SingleGame::move(int player, sf::Vector2i moveTo)
{
	if(moveTo.x < 0 || moveTo.x > fieldSize.x || moveTo.y < 0 || moveTo.y > fieldSize.y)
		return "INVALID_MOVE";
		
	sf::Vector2i displacement = moveTo - players[player].position;
	int distanceSquared = displacement.x * displacement.x + displacement.y * displacement.y;
		
	if(distanceSquared > moveDistance * moveDistance)
		return "INVALID_MOVE";
		
	if(getVisible(players[player].position, moveTo) != moveTo)
		return "INVALID_MOVE";
		
	players[player].nextMove = moveTo;
	return "OK";
}
///////////////////////////////////////////////////////////

std::string SingleGame::shoot(int player)
{
	if(players[player].ammo == 0)
		return "NO_AMMO";
	
	players[player].ifShoot = true;
	return "OK";
}
//////////////////////////////////////////////////////////

std::string SingleGame::getRound()
{
	return "OK\n" + std::to_string(roundCount) + "\n" + std::to_string(currentRound);
}
//////////////////////////////////////////////////////////////

std::string SingleGame::getAmmo(int player)
{
	return "OK\n" + std::to_string(players[player].ammo);
}
/////////////////////////////////////////////////////////////////

std::string SingleGame::getPoints(int player)
{
	int player2 = player == 0? 1: 0;
	
	return "OK\n" + std::to_string(players[player].points) + "\n" + std::to_string(players[player2].points);
}
//////////////////////////////////////////////////////////////////

std::string SingleGame::getGameInfo()
{
	return "OK\n" + std::to_string(moveDistance);
}
////////////////////////////////////////////////////////////////

//functions for calculating visibility of field

sf::Vector2i SingleGame::getVisible(sf::Vector2i pos1, sf::Vector2i pos2)
{
	sf::Vector2i field = getNextField(pos1, pos1, pos2);

	while(field != pos2)
	{
		if(field.x < 0 || field.x > fieldSize.x || field.y < 0 || field.y > fieldSize.y)
			return sf::Vector2i(-1, -1);
			
		if(this->field[field.x + field.y * fieldSize.x] != Field::Empty)
			return field;
			
		field = getNextField(pos1, field, pos2);
	}
	
	return field;
}
///////////////////////////////////////////////////////////////

sf::Vector2i SingleGame::getNextField(sf::Vector2i starting, sf::Vector2i current, sf::Vector2i target)
{
	if(current == target)
		return target;
		
	sf::Vector2i direction(0, 0);
	if (target.x > starting.x)
		direction.x = 1;
	else if (target.x < starting.x)
		direction.x = -1;
	if (target.y > starting.y)
		direction.y = 1;
	else if (target.y < starting.y)
		direction.y = -1;

	//horizontal
	if (direction.y == 0)
		return sf::Vector2i(current.x < target.x? current.x+1: current.x-1, current.y);

	//vertical
	if(direction.x == 0)
		return sf::Vector2i(current.x, current.y < target.y? current.y+1: current.y-1);
	
	const sf::Vector2i movedX(current.x + direction.x, current.y);
	if(intersects(starting, movedX, target))
		return movedX;

	const sf::Vector2i movedY(current.x, current.y + direction.y);
	if(intersects(starting, movedY, target))
		return movedY;

	return current + direction;
}
/////////////////////////////////////////////////////////

bool SingleGame::intersects(sf::Vector2i starting, sf::Vector2i current, sf::Vector2i target)
{
	sf::Vector2i a = target - starting;
	sf::Vector2i b = (current - starting) * 2;
	int yxm = a.y * (b.x - 1);
	int yxp = a.y * (b.x + 1);
	int xym = a.x * (b.y - 1);
	int xyp = a.x * (b.y + 1);
	return a.x * a.y > 0? yxm < xyp && yxp > xym: yxm > xym && yxp < xyp;
}
