#ifndef SINGLE_GAME_HPP
#define SINGLE_GAME_HPP

#include <vector>
#include <string>

#include <SFML/System/Vector2.hpp>
#include <SFML/System/Clock.hpp>

class GameManager;

class SingleGame
{
public:

	void init(sf::Vector2i size, int initAmmo, int roundCount, int moveDistance, float roundDuration);
	
	
	//update returns:
	//-1 - round still lasts
	//0 - round passed
	//1 - round passed, end of the game
	int update();
	
	std::string getField(int player);
	std::string move(int player, sf::Vector2i moveTo);
	std::string shoot(int player);
	std::string getRound();
	std::string getAmmo(int player);
	std::string getPoints(int player);
	std::string getGameInfo();
	
	//returns coordinates of first obsticle, (-1, -1) if out of bounds or pos2 if pos2 is visible from pos1
	sf::Vector2i getVisible(sf::Vector2i pos1, sf::Vector2i pos2);
	static sf::Vector2i getNextField(sf::Vector2i starting, sf::Vector2i current, sf::Vector2i target);
	static bool intersects(sf::Vector2i starting, sf::Vector2i current, sf::Vector2i target);
	
	struct Player
	{
		sf::Vector2i position;
		int ammo;
		
		sf::Vector2i nextMove;
		bool ifShoot;
		
		int points;
	};
	
	enum Field
	{
		Empty,
		Player1,
		Player2,
		Obsticle
	};
	
private:

	Player players[2];
	
	sf::Vector2i fieldSize;
	std::vector<Field> field;
	int obsticleCount;
	
	int initialAmmo;
	
	int roundCount;
	int currentRound;
	
	int moveDistance;
	
	float roundDuration;
	sf::Clock roundTimer;
	
	friend GameManager;
};

#endif//SINGLE_GAME_HPP
