#ifndef SOCKET_SERVER_HPP
#define SOCKET_SERVER_HPP

#include <SFML/Network.hpp>

class SocketServer
{
public:

	SocketServer(unsigned short port);
	
	bool listen();
	bool connect(size_t socketId);
	void stopListening();
	
	bool send(std::string data, size_t socketId);
	std::string read(size_t socketId);
	
	const sf::TcpSocket &operator[](size_t socketId)const;
	
protected:

	sf::TcpSocket socket[2];
	sf::TcpListener listener;

private:

	unsigned short port;

};

#endif//SOCKET_SERVER_HPP
