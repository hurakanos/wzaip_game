#ifndef USER_HPP
#define USER_HPP

#include <string>

#include <SFML/Network/TcpSocket.hpp>

#include <GameSocket.hpp>

class GameManager;
class UserManager;

class User
{
public:

	void receive(GameSocket *socket);

	void endTurn(std::string status);
	
	bool isInGame(){ return inGame; };
	bool isWaitingForGame(){ return waitForStart; };
	void startGame();
	
	std::string getLogin(){ return login; };
	
private:
	GameManager *gameManager;
	UserManager *userManager;
	
	std::string login;
	std::string password;
	
	bool inGame{0};
	bool waitForStart{0};
	
	friend UserManager;
};

#endif//USER_HPP
