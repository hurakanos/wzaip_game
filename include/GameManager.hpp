#ifndef GAME_MANAGER_HPP
#define GAME_MANAGER_HPP

#include <vector>

#include <SFML/System/Vector2.hpp>

#include <SingleGame.hpp>
#include <User.hpp>

class GameManager
{
public:
	
	void createGameRoom(User *player1, User *player2);
	
	std::string parseCommand(std::string &command, User *player);
	
	void update();

	struct GameRoom
	{
		SingleGame game;
		User *players[2];
	};
private:

	std::vector<GameRoom> gameRooms;
};

#endif//GAME_MANAGER_HPP
