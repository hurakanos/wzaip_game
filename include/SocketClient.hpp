#ifndef SOCKET_CLIENT_HPP
#define SOCKET_CLIENT_HPP

#include <string>
#include<SFML/Network.hpp>

class SocketClient
{
public:
	
	SocketClient(std::string ipAddress, unsigned short port);
	
	bool connect();
	
	bool send(std::string data);
	std::string read();
	
private:

	sf::TcpSocket socket;
	sf::IpAddress ipAddress;
	unsigned short port;
};

#endif//SOCKET_CLIENT_HPP
