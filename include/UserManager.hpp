#ifndef USER_MANAGER_HPP
#define USER_MANAGER_HPP

#include <vector>
#include <map>
#include <list>
#include <memory>
#include <string>

#include <SFML/Network.hpp>

#include <GameSocket.hpp>

class User;
class GameManager;

class UserManager
{
public:

	UserManager(unsigned short port, GameManager *gameManager);
	
	void addUser(std::string login, std::string password);

	void update();
	
	std::vector<std::unique_ptr<User>> &getUsers(){ return users; };
	std::vector<GameSocket*> getUserSockets(User *user);
	
private:

	std::vector<std::unique_ptr<User>> users;
	std::list<std::unique_ptr<GameSocket>> sockets;
	std::map<GameSocket*, User*> socketUserMap;
	
	sf::TcpListener listener;
	unsigned short port;
	sf::SocketSelector selector;
	
	void handleLogin(GameSocket *socket);
	
	GameManager *gameManager;
};

#endif//USER_MANAGER_HPP
