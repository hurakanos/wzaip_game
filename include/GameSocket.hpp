#ifndef GAME_SOCKET_HPP
#define GAME_SOCKET_HPP

#include <string>

#include <SFML/Network/TcpSocket.hpp>

class UserManager;

class GameSocket
{
public:

	std::string read();
	void send(std::string data);

	void disconnect(){ disconnected = true; };
	bool isDisconnected(){ return disconnected; };
	bool &waiting(){ return isWaiting; };
		
	
private:
	
	sf::TcpSocket socket;
	bool disconnected{0};
	bool isWaiting{0};
	
	friend UserManager;
};

#endif//GAME_SOCKET_HPP
