#ifndef GAME_SERVER_HPP
#define GAME_SERVER_HPP

#include <SocketServer.hpp>

#include <string>
#include <SFML/System/Vector2.hpp>

class GameServer: public SocketServer
{
public:
	
	GameServer(sf::Vector2i fieldSize, unsigned short port);
	~GameServer();
	
	void initialize();
	void run();
	std::string getField(unsigned firstPlayer);
	
private:
	sf::Vector2i fieldSize;
	char *field{nullptr};
	int obsticleCount;
	int winner{0};
	
	struct Player
	{
		sf::Vector2i currentPosition;
		sf::Vector2i moveDirection;
		sf::Vector2i shootDirection;
		bool moveThisTurn;
		bool waiting;
		const GameServer *game;
		
		int move(sf::Vector2i dir, sf::Vector2i shoot);
	};
	Player players[2];
	
	void executeCommand(unsigned player);
	void endTurn();
	void checkCollision(unsigned player, float &distance, int &collisionType, sf::Vector2i &pos);
	
	std::string read(size_t socketId);
	
	friend GameServer::Player;
};

#endif//GAME_SERVER_HPP
