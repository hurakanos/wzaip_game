#include <iostream>
#include <vector>
#include <sstream>

#include <SocketClient.hpp>

using namespace std;

struct GameStatus
{
	vector<char> field;//0 - empty; 1 - player1, 2 - player2, 3 - obsticle
	int sizeX, sizeY;
	int posX, posY;
	int oppPosX, oppPosY;
	int obsticleCount;
	int ammo;
	int points, oppPoints;
};
////////////////////////////////////////////

void getField(string &str, GameStatus &game);

/////////////////////////////////////////

int main(int argc, char **argv)
{	
	string ipAddress{"127.0.0.1"};
	short port{6969};
	GameStatus game;
	stringstream stream;
	string respond;
	
	string login, passwd;
	if(argc < 3)
		return 1;
	login = argv[1];
	passwd = argv[2];
	
	if(argc == 5)
	{
		ipAddress = argv[3];
		port = stoi(argv[4]);
	}
		
	SocketClient clnt(ipAddress, port);

//connecting with socket		
	if(!clnt.connect())
	{
		cout<<"Could't connect with socekt\n";
		return 1;
	}

//starting game
	respond = clnt.read();
	if(respond.find("LOGIN") == string::npos)
		return 1;
	clnt.send(login + " " + passwd);
	
	respond = clnt.read();
	cout<<respond<<"\n";
	if(respond.find("OK") == string::npos)
		return 1;
	
//get field
	clnt.send("GET_FIELD");
	respond = clnt.read();
	
	getField(respond, game);
	
//get ammo
	clnt.send("GET_AMMO");
	respond = clnt.read();	
	stream.str(respond);
	stream.ignore(10, '\n');
	stream>>game.ammo;
	
//main game loop
	while(true)
	{
		int moveX{game.posX}, moveY{game.posY};
		
		if(game.posX != game.sizeX-1 && game.field[game.posX + 1 + game.posY * game.sizeX] == 0)
			moveX++;
		else if(game.posX != 0 && game.field[game.posX - 1 + game.posY * game.sizeX] == 0)
			moveX--;
		else if(game.posY != game.sizeY-1 && game.field[game.posX + (game.posY+1) * game.sizeX] == 0)
			moveY++;
		else if(game.posY != 0 && game.field[game.posX + (game.posY-1) * game.sizeX] == 0)
			moveY--;
			
		clnt.send("MOVE "+to_string(moveX)+" "+to_string(moveY));
		clnt.read();//read OK and ignore
		
		clnt.send("SHOOT");
		clnt.read();//read OK and ignore
		
		clnt.send("WAIT");
		respond = clnt.read();
		if(respond.size() < 4)
			respond = clnt.read();
			
		if(respond.find("GAME_OVER") != string::npos)
		{
			stream.str(respond);
			stream.ignore(10, '\n');//ignore GAME_OVER text
			stream>>game.points>>game.oppPoints;
			break;
		}
		
		clnt.send("GET_FIELD");
		respond = clnt.read();
		getField(respond, game);
	}
	
	cout<<game.points<<"\n"<<game.oppPoints<<"\n";
		
	return 0;
}
/////////////////////////////////////////////////////

void getField(string &str, GameStatus &game)
{
	stringstream stream;
	stream.str(str);
	stream.ignore(10, '\n');//getting rid of "OK"
	stream >> game.sizeX >> game.sizeY;
	game.field.resize(game.sizeX * game.sizeY);

	for(char &x: game.field)//clear field
		x = 0;
	
	stream>>game.posX>>game.posY;
	stream>>game.oppPosX>>game.oppPosY;
	
	game.field[game.posX + game.posY * game.sizeX] = 1;
	game.field[game.oppPosX + game.oppPosY * game.sizeX] = 2;
	
	stream>>game.obsticleCount;
	for(int i = 0; i < game.obsticleCount; i++)
	{
		int x, y;
		stream>>x>>y;
		game.field[x + y * game.sizeX] = 3;
	}
}
