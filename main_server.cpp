#include <string>
#include <iostream>
#include <vector>
#include <fstream>


#include <SFML/System/Vector2.hpp>

#include <GameManager.hpp>
#include <UserManager.hpp>

using namespace std;

int main(int argc, char **argv)
{
	short port;

	if(argc != 2)
	{
		cout<<"Please add config file as argument\n";
		return 1;
	}
	
	//config file structure: first line ethernet port number
	//second line number of users to create
	//next userCount lines login <space> password of single user
	ifstream file;
	file.open(argv[1]);
	if(!file.is_open())
	{
		cout<<"Error opening config file\n";
		return 1;
	}
	
	
	file>>port;
	
	
	
	GameManager gameManager;
	UserManager userManager(port, &gameManager);
	
	int userCount;
	file>>userCount;
	for(int i = 0; i < userCount; i++)
	{
		std::string login, passwd;
		file>>login>>passwd;
		
		userManager.addUser(login, passwd);
	}
	
	while(true)
	{
		gameManager.update();
		userManager.update();

		for(auto &user1: userManager.getUsers())
		{
			for(auto &user2: userManager.getUsers())
			{
				if(user1.get() != user2.get() && user1->isWaitingForGame() && user2->isWaitingForGame())
				{
					gameManager.createGameRoom(user1.get(), user2.get());
				}
			}
		}
	}
	
	return 0;
}
