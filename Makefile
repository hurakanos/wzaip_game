CLIENT_SOURCE = src/SocketClient.cpp
CLIENT_HEADER = include/SocketClient.hpp

SERVER_SOURCE = src/GameManager.cpp src/SingleGame.cpp src/User.cpp src/UserManager.cpp src/GameSocket.cpp
SERVER_HEADER = include/GameManager.hpp include/SingleGame.hpp include/User.hpp include/UserManager.hpp include/GameSocket.hpp

FLAGS = -std=c++14 -I./include
LIBS = -lsfml-network -lsfml-system

all: client server

client:main_client.cpp ${CLIENT_SOURCE} ${CLIENT_HEADER}
	g++ -o $@ $< ${CLIENT_SOURCE} ${FLAGS} ${LIBS}
	
server:main_server.cpp ${SERVER_SOURCE} ${SERVER_HEADER}
	g++ -o $@ $< ${SERVER_SOURCE} ${FLAGS} ${LIBS}

